
#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <PubSubClient.h>



#define DEBUG 1
#define MAX_SRV_CLIENTS 1
IPAddress zero(0, 0, 0, 0);

//Broker informations

//String ssid = "Genie SmartHome";              String pass = "w72i61m1";
String ssid = "geniesmart";                   String pass = "12341234";
String user_router_ssid = "userRouter";                 String user_router_pass = "userRouterPass";
String user_mobile_ssid = "safeSSID";                 String user_mobile_pass = "safePass";
String panel_config = "ABC-12-123456-GTX11";                     String hub_ip = "192.168.0.119";
//panel topic information
String panel_topic = "test";
String out_topic = "test/out";
String in_topic = "test/in";
//1-20 : panel config
//21-52:ssid

int panel_config_add = 1;                    int panel_config_length_add = 231;
int user_router_ssid_add = 21;                 int user_router_ssid_length_add = 232;
int user_router_pass_add = 54;                int user_router_pass_length_add = 233;
int user_mobile_ssid_add = 86;                int user_mobile_ssid_length_add = 234;
int user_mobile_pass_add = 119;                int user_mobile_pass_length_add = 235;
int panel_topic_add = 152;                    int panel_topic_length_add = 236;
int in_topic_add = 173;                       int in_topic_length_add = 237;
int out_topic_add = 194;                      int out_topic_length_add = 238;
int hub_ip_add = 215;                         int hub_ip_length_add = 239;
//
uint8_t i = 0;
//delay


WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
WiFiServer server(23);
WiFiClient serverClients[MAX_SRV_CLIENTS];


void setup() {

  unsigned long currentMillis = millis();
  Serial.begin(9600);
  EEPROM.begin(512);
  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);

  //c
  writeThisStringToEEPROM(panel_config, panel_config_add);     //start :1-20
  writeThisIntToEEPROM(panel_config.length(), panel_config_length_add);     //start :1-20

  //s
  writeThisStringToEEPROM(user_router_ssid, user_router_ssid_add);     //start :1-20
  writeThisIntToEEPROM(user_router_ssid.length(), user_router_ssid_length_add);     //start :1-20
  writeThisStringToEEPROM(user_router_pass, user_router_pass_add);     //start :21-40
  writeThisIntToEEPROM(user_router_pass.length(), user_router_pass_length_add);     //start :1-20
  //p
  writeThisStringToEEPROM(user_mobile_ssid, user_mobile_ssid_add);    //start 41-60
  writeThisIntToEEPROM(user_mobile_ssid.length(), user_mobile_ssid_length_add);     //start :1-20
  writeThisStringToEEPROM(user_mobile_pass, user_mobile_pass_add);    //start 61-80
  writeThisIntToEEPROM(user_mobile_pass.length(), user_mobile_pass_length_add);     //start :1-20

  //t
  writeThisStringToEEPROM(panel_topic, panel_topic_add);     //start :1-20
  writeThisIntToEEPROM(panel_topic.length(), panel_topic_length_add);     //start :1-20
  writeThisStringToEEPROM(in_topic, in_topic_add);     //start :1-20
  writeThisIntToEEPROM(in_topic.length(), in_topic_length_add);
  writeThisStringToEEPROM(out_topic, out_topic_add);     //start :1-20
  writeThisIntToEEPROM(out_topic.length(), out_topic_length_add);
  writeThisStringToEEPROM(hub_ip, hub_ip_add);     //start :1-20
  writeThisIntToEEPROM(hub_ip.length(), hub_ip_length_add);

}

void loop() {

  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);


  server.begin();
  server.setNoDelay(false);
  cycle();
  delay(2000);
}

void writeThisStringToEEPROM(String anyString, int startAddress)
{
  uint8_t stringStart = 0;
  for (i = startAddress; i < (startAddress + anyString.length()); i++)
  {
    EEPROM.write(i, anyString[stringStart]);
    stringStart++;
  }
  EEPROM.commit();
}

void writeThisIntToEEPROM(int value, int startAddress)
{
  EEPROM.write(startAddress, value);
  EEPROM.commit();
}


String readFromAddress(int startAddress, int lengthOfString)
{
  String p = "";
  for (i = startAddress; i < (startAddress + lengthOfString); ++i)
  {
    p = p + (char)EEPROM.read(i);

  }
  return p;
}



int readIntFromAddress(int intAddress)
{
  uint8_t p;
  p = (uint8_t)EEPROM.read(intAddress);
  return p;
}

bool connectTo(String any_ssid, String any_password)
{
  char * anySSID = (char *)any_ssid.c_str();
  char * anyPASS = (char *)any_password.c_str();
  WiFi.begin(anySSID, anyPASS);
  WiFi.hostname("Genie_P");
  if (DEBUG == 1)
  {
    Serial.print("\nConnecting to "); Serial.println(any_ssid);
  }
  uint8_t o = 0;
  while (WiFi.status() != WL_CONNECTED && o++ < 20) {
    delay(500);
  }
  if (o == 21) {
    Serial.print("Could not connect to"); Serial.println(any_ssid);
  }
  if (o <= 20) {
    Serial.print(WiFi.localIP());
    hub_ip = readFromAddress(hub_ip_add, readIntFromAddress(hub_ip_length_add));
    connectToBroker(hub_ip);
    return 1;
  }
  else {
    return 0;
  }

}


void connectToBroker(String hub_ip_local)
{
  Serial.print("setting MQTT");
  char * hubIp = (char *)hub_ip_local.c_str();
  client.setServer(hubIp, 1883);
  client.setCallback(callback);

  if (!client.connected()) {
    Serial.print("hey this is it");
    reconnect( hub_ip_local);
  }
  else
  {
    ESP.wdtFeed();
    Serial.print("ESP in network");
    yield();
  }
}

void reconnect(String hub_ip_local) {

  while (!client.connected()) {
    out_topic = readFromAddress(in_topic_add, readIntFromAddress(in_topic_length_add));
    in_topic = readFromAddress(out_topic_add, readIntFromAddress(out_topic_length_add));
    panel_config = readFromAddress(panel_config_add, readIntFromAddress(panel_config_length_add)); //readIntFromAddress(panel_config_length_add));
    hub_ip = readFromAddress(hub_ip_add, readIntFromAddress(hub_ip_length_add)); //readIntFromAddress(panel_config_length_add));


    Serial.print("setting this MQTT");
    char * hubIp = (char *)hub_ip.c_str();
    client.setServer(hubIp, 1883);
    client.setCallback(callback);

    if (client.connect((char *)panel_config.c_str())) {
      client.publish((char *)out_topic.c_str(), "connected");
      client.subscribe((char *)in_topic.c_str());
    } else {
      break;
    }
  }


}




void cycle()

{
  connectTo(ssid, pass);
  while (isConnected()) {
    client.loop();
    ESP.wdtFeed();
    delay(300);
    yield();
  }
  connectTo(readFromAddress(user_router_ssid_add, readIntFromAddress(user_router_ssid_length_add)), readFromAddress(user_router_pass_add, readIntFromAddress(user_router_pass_length_add)));
  while (isConnected()) {
    client.loop();
    ESP.wdtFeed();
    delay(300);
    yield();
  }
  connectTo(readFromAddress(user_mobile_ssid_add, readIntFromAddress(user_mobile_ssid_length_add)), readFromAddress(user_mobile_pass_add, readIntFromAddress(user_mobile_pass_length_add)));
  while (isConnected()) {
    client.loop();
    ESP.wdtFeed();
    delay(300);
    yield();
  }
  delay(3000);

}

bool isConnected()
{
  if (WiFi.localIP() != zero)
  {
    uint8_t i;
    //stay connected toMQTT
    if (!client.connected()) {
      hub_ip = readFromAddress(hub_ip_add, readIntFromAddress(hub_ip_length_add));
      reconnect(hub_ip);
    }

    //
    //check if there are any new clients
    if (server.hasClient()) {
      for (i = 0; i < MAX_SRV_CLIENTS; i++) {
        //find free/disconnected spot
        if (!serverClients[i] || !serverClients[i].connected()) {
          if (serverClients[i]) serverClients[i].stop();
          serverClients[i] = server.available();
          continue;
        }
      }
      //no free/disconnected spot so reject
      WiFiClient serverClient = server.available();
      serverClient.stop();
    }
    //check clients for data
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
      if (serverClients[i] && serverClients[i].connected()) {
        if (serverClients[i].available()) {
          //get data from the telnet client and push it to the UART
          while (serverClients[i].available())
          { serverClients[i].println("2017(c)Genie SmartHome Ver: 1.0");
            if (serverClients[i].find(':'))
            {

              char c = serverClients[i].read();
              switch (c) {
                case '!':
                  { ESP.wdtFeed();
                    uint8_t t = 1;
                    Serial.write(serverClients[i].parseInt());
                    break;
                  }
                case 'f':
                  { ESP.wdtFeed();
                    serverClients[i].print(readFromAddress(panel_config_add, readIntFromAddress(panel_config_length_add)));
                    serverClients[i].print('-');
                    serverClients[i].print(readFromAddress(user_router_ssid_add, readIntFromAddress(user_router_ssid_length_add)));
                    serverClients[i].print('-');
                    serverClients[i].print(readFromAddress(user_router_pass_add, readIntFromAddress(user_router_pass_length_add)));
                    serverClients[i].print('-');
                    serverClients[i].print(readFromAddress(user_mobile_ssid_add, readIntFromAddress(user_mobile_ssid_length_add)));
                    serverClients[i].print('-');
                    serverClients[i].print(readFromAddress(user_mobile_pass_add, readIntFromAddress(user_mobile_pass_length_add)));
                    serverClients[i].print('-');
                    serverClients[i].print(readFromAddress(panel_topic_add, readIntFromAddress(panel_topic_length_add)));
                    serverClients[i].print('-');
                    serverClients[i].print(readFromAddress(in_topic_add, readIntFromAddress(in_topic_length_add)));
                    serverClients[i].print('-');
                    serverClients[i].print(readFromAddress(out_topic_add, readIntFromAddress(out_topic_length_add)));
                    serverClients[i].print('-');
                    serverClients[i].print(readFromAddress(hub_ip_add, readIntFromAddress(hub_ip_length_add)));
                    break;
                  }
                case 'c':
                  { ESP.wdtFeed();
                    panel_config = serverClients[i].readStringUntil(',');
                    writeThisStringToEEPROM(panel_config, panel_config_add);     //start :1-20
                    writeThisIntToEEPROM(panel_config.length(), panel_config_length_add);     //start :1-20
                    break;
                  }
                case 's':
                  { ESP.wdtFeed();
                    user_router_ssid = serverClients[i].readStringUntil(',');
                    user_router_pass = serverClients[i].readStringUntil(',');
                    writeThisStringToEEPROM(user_router_ssid, user_router_ssid_add);     //start :1-20
                    writeThisIntToEEPROM(user_router_ssid.length(), user_router_ssid_length_add);     //start :1-20
                    writeThisStringToEEPROM(user_router_pass, user_router_pass_add);     //start :21-40
                    writeThisIntToEEPROM(user_router_pass.length(), user_router_pass_length_add);     //start :1-20
                    break;
                  }
                case 't':
                  { ESP.wdtFeed();
                    //panel topic infos
                    panel_topic = serverClients[i].readStringUntil(',');
                    in_topic = serverClients[i].readStringUntil(',');
                    out_topic = serverClients[i].readStringUntil(',');
                    hub_ip = serverClients[i].readStringUntil(',');
                    //saving panel topics
                    writeThisStringToEEPROM(panel_topic, panel_topic_add);     //start :1-20
                    writeThisIntToEEPROM(panel_topic.length(), panel_topic_length_add);     //start :1-20
                    writeThisStringToEEPROM(in_topic, in_topic_add);     //start :1-20
                    writeThisIntToEEPROM(in_topic.length(), in_topic_length_add);
                    writeThisStringToEEPROM(out_topic, out_topic_add);     //start :1-20
                    writeThisIntToEEPROM(out_topic.length(), out_topic_length_add);
                    writeThisStringToEEPROM(hub_ip, hub_ip_add);     //start :1-20
                    writeThisIntToEEPROM(hub_ip.length(), hub_ip_length_add);

                    hub_ip = readFromAddress(hub_ip_add, readIntFromAddress(hub_ip_length_add));

                    connectToBroker(hub_ip);
                    break;
                  }
                case 'p':
                  { ESP.wdtFeed();
                    user_mobile_ssid = serverClients[i].readStringUntil(',');
                    user_mobile_pass = serverClients[i].readStringUntil(',');
                    writeThisStringToEEPROM(user_mobile_ssid, user_mobile_ssid_add);    //start 41-60
                    writeThisIntToEEPROM(user_mobile_ssid.length(), user_mobile_ssid_length_add);     //start :1-20
                    writeThisStringToEEPROM(user_mobile_pass, user_mobile_pass_add);    //start 61-80
                    writeThisIntToEEPROM(user_mobile_pass.length(), user_mobile_pass_length_add);     //start :1-20
                    break;
                  }
                case 'x':
                  {
                    int j = 0;
                    for (j = 0; j < 250; j++)
                    { EEPROM.write(j, 0);
                      ESP.wdtFeed();
                    }
                    break;
                  }
                case 'r':
                  { ESP.wdtFeed();
                    ESP.restart();
                    break;
                  }
                case 'h':
                  { ESP.wdtFeed();

                    serverClients[i].println("Options : ");
                    serverClients[i].println(":c - configuring panel");
                    serverClients[i].println(":s - router information");
                    serverClients[i].println(":p - safety information");
                    serverClients[i].println(":x - clear memory");
                    serverClients[i].println(":r - restart panel");
                    serverClients[i].println(":h - help");
                    serverClients[i].println(":! - command to controller");
                    serverClients[i].println(":f - Retrive information");
                    break;
                  }
                default:
                  { ESP.wdtFeed();
                    serverClients[i].println("2017(c)Genie SmartHome Ver: 1.0");

                    break;
                  }
              }
            }
          }
        }
      }
    }
    return 1;
  }
  else {
    return 0;
  }
}


void callback(char* topic, byte * payload, unsigned int length) {
  Serial.write(payload[0]);
  Serial.write(payload[1]);
  Serial.write((*(payload + 2)) & 0b00000111);
  int a = ((int)(*(payload + 3)) & 0b00001111);
  int b = ((int)(*(payload + 4)) & 0b00001111);
  Serial.write((10 * a) + b);
  Serial.write((*(payload + 5)) & 0b00000111);
  Serial.write(payload[6]);
  // Serial.println();
}




