/*
   Genie IOT Control Board.c

   Created: 21-12-2016 20:07:24
   Adapted by : Ajinkya kohokade
*/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <avr/wdt.h>
#include <avr/delay.h>

#define Dimmer1 PINC5
#define Dimmer2 PINC4
#define Relay4  PINC3
#define Relay3  PINC2
#define Relay2  PINC1
#define Relay1  PINC0

#define Switch1 PB0
#define Switch2 PB1
#define Switch3 PB2
#define Switch4 PB3
#define Switch5 PB4
#define Switch6 PB5

//Debounce Threshold Delay Should be greater than Debounce Delay
#define Debounce_Delay 5000
#define Debounce_Threshold_Delay 6000

//Present Values should be between 0 - 100
#define Dimming_Preset_1 20
#define Dimming_Preset_2 38
#define Dimming_Preset_3 66

//Device Control Bit
#define Read_Byte      '^'
#define Start_Byte      '['
#define Stop_Byte     ']'
#define Buffer_Clear_Byte '$'
#define End_Byte      '|'

bool Switch1_Lock = 0;
bool Switch2_Lock = 0;
bool Switch3_Lock = 0;
bool Switch4_Lock = 0;
bool Switch5_Lock = 0;
bool Switch6_Lock = 0;

bool Switch1_State = 0;
bool Switch2_State = 0;
bool Switch3_State = 0;
bool Switch4_State = 0;
bool Switch5_State = 0;
bool Switch6_State = 0;

bool Device1_State = 0;
bool Device2_State = 0;
bool Device3_State = 0;
bool Device4_State = 0;
uint8_t Device5_State = 0;
uint8_t Device6_State = 0;

bool Dimmer1_Control = 0;
bool Dimmer2_Control = 0;

uint8_t Dimmer1_Value = 0;
uint8_t Dimmer2_Value = 0;

uint8_t volatile Data_Buffer[5] = { 0, 0, 0, 0, 0 };
uint8_t volatile Sending_Data_Buffer[5] = { 0,0, 0, 0, 0};
volatile static uint8_t Sending_Buffer_Position = 0;
volatile static uint8_t Data_Position = 0;
volatile static uint8_t Data_Processing = 0;

volatile static bool Send_Device_State = 0;
volatile static bool Transmiting = 0;

bool Debounce_Check = 0;
uint16_t Debounce_Counter = 0;

bool Switch1_Debounce_State = 0;
bool Switch2_Debounce_State = 0;
bool Switch3_Debounce_State = 0;
bool Switch4_Debounce_State = 0;
bool Switch5_Debounce_State = 0;
bool Switch6_Debounce_State = 0;

uint8_t Dimming1_Preset_Position = 0;
uint8_t Dimming2_Preset_Position = 0;

void External_Interrupt_Enable() {
  MCUCR |= (1 << ISC01) | (1 << ISC00);
  GICR |= (1 << INT0);
}

ISR(INT0_vect) {
  TCNT0 = 0;
  TCCR0 |= (1 << CS00) | (1 << CS02);
}

void Timer_Init() {
  TCCR0 |= (1 << CS00) | (1 << CS02);
}

void UART_Init(uint16_t UBRR_Value) {
  UBRRL = UBRR_Value;
  UBRRH = UBRR_Value >> 8;

  UCSRB = 0b11011000;
  UCSRC = 0b10000110;
}

ISR(USART_RXC_vect) {
  uint8_t UDR_Ref = UDR;

  if (UDR_Ref == Read_Byte) {
    //Send_Device_State = 1;
    //Transmiting = 0;
    volatile uint8_t data = 0;
    volatile uint8_t D5 = 0;
    volatile uint8_t D6 = 0;
    volatile uint8_t LOCK=0;
    
    //data = (1<< 0)|(Device1_State << 1) | (Device2_State << 2) | (Device3_State << 3) | (Device4_State << 4) | (Dimmer1_Control << 5) | (Dimmer2_Control << 6);
    data = (1<< 0)|(Dimmer2_Control << 1) | (Dimmer1_Control << 2) | (Device4_State << 3) | (Device3_State << 4) | (Device2_State << 5) | (Device1_State << 6);

    if (Device5_State == 0) {
      D5 = '*';
    } else {
      D5 = Device5_State;
    }
    if (Device6_State == 0) {
      D6 = '*';
    } else {
      D6 = Device6_State;
    }

   //LOCK = (1<< 0) | (Switch1_Lock << 1) | (Switch2_Lock << 2) | (Switch3_Lock << 3) | (Switch4_Lock << 4) | (Switch5_Lock << 5) | (Switch6_Lock << 6);
   LOCK = (1<< 0) | (Switch6_Lock << 1) | (Switch5_Lock << 2) | (Switch4_Lock << 3) | (Switch3_Lock << 4) | (Switch2_Lock << 5) | (Switch1_Lock << 6);
   
    Send_Device_State = 1;
    Transmiting == 0;
    Send_Switch_State(data, D5, D6 ,LOCK) ;
  }


  if (UDR_Ref == Buffer_Clear_Byte) {
    Data_Position = 0;
    memset(&Data_Buffer, 0, 5);
  } else {
    if (Data_Position == 5 && Data_Processing == 0) {
      Data_Position = 0;
      memset(&Data_Buffer, 0, 5);
    }
    Data_Buffer[Data_Position] = UDR_Ref;
    Data_Position++;
  }
}


ISR(USART_TXC_vect) {
  if (Sending_Buffer_Position >= 4) {
    Sending_Buffer_Position = 0;
    Transmiting = 0;
  } else {
    Sending_Buffer_Position++;
    UART_TXC(Sending_Data_Buffer[Sending_Buffer_Position]);
  }
}

void UART_TXC(uint8_t send_data) {
  UDR = send_data;
}

void UART_Date_Processing() {

  if (Data_Buffer[4] == Stop_Byte) {
    if (Data_Buffer[0] == Start_Byte) {
      Transmiting = 0;
      switch (Data_Buffer[1]) {
        case 1:
          if (Data_Buffer[2] < 1) {
            Device1_State = 0;
            PORTC &= ~(1 << Relay1);
          } else if (Data_Buffer[2] >= 1) {
            Device1_State = 1;
            PORTC |= (1 << Relay1);
          }
          Switch1_Lock = Data_Buffer[3];
          break;
        case 2:
          if (Data_Buffer[2] < 1) {
            Device2_State = 0;
            PORTC &= ~(1 << Relay2);
          } else if (Data_Buffer[2] >= 1) {
            Device2_State = 1;
            PORTC |= (1 << Relay2);
          }
          Switch2_Lock = Data_Buffer[3];
          break;
        case 3:
          if (Data_Buffer[2] < 1) {
            Device3_State = 0;
            PORTC &= ~(1 << Relay3);
          } else if (Data_Buffer[2] >= 1) {
            Device3_State = 1;
            PORTC |= (1 << Relay3);
          }
          Switch3_Lock = Data_Buffer[3];
          break;
        case 4:
          if (Data_Buffer[2] < 1) {
            Device4_State = 0;
            PORTC &= ~(1 << Relay4);
          } else if (Data_Buffer[2] >= 1) {
            Device4_State = 1;
            PORTC |= (1 << Relay4);
          }
          Switch4_Lock = Data_Buffer[3];
          break;
        case 5:
          if (Data_Buffer[2] < 4) {
            Device5_State = 0;
            Dimmer1_Value = 0;
            Dimmer1_Control = 0;
            PORTC &= ~(1 << Dimmer1);
          } else if (Data_Buffer[2] > 97 && Data_Buffer[2] <= 100) {
            Device5_State = 100;
            Dimmer1_Value = 100;
            Dimmer1_Control = 0;
            PORTC |= (1 << Dimmer1);
          } else {
            Device5_State = Data_Buffer[2];
            Dimmer1_Control = 1;
            Dimmer1_Value = ((100 - Data_Buffer[2]) * 1.5);
          }
          Switch5_Lock = Data_Buffer[3];

          if (Device5_State < Dimming_Preset_1) {
            Dimming1_Preset_Position = 0;
          } else if (Device5_State >= Dimming_Preset_1
                     && Device5_State < Dimming_Preset_2) {
            Dimming1_Preset_Position = 1;
          } else if (Device5_State >= Dimming_Preset_2
                     && Device5_State < Dimming_Preset_3) {
            Dimming1_Preset_Position = 2;
          } else {
            Dimming1_Preset_Position = 3;
          }
          break;
        case 6:
          if (Data_Buffer[2] < 4) {
            Device6_State = 0;
            Dimmer2_Value = 0;
            Dimmer2_Control = 0;
            PORTC &= ~(1 << Dimmer2);
          } else if (Data_Buffer[2] > 97 && Data_Buffer[2] <= 100) {
            Device6_State = 100;
            Dimmer2_Value = 100;
            Dimmer2_Control = 0;
            PORTC |= (1 << Dimmer2);
          } else {
            Device6_State = Data_Buffer[2];
            Dimmer2_Control = 1;
            Dimmer2_Value = ((100 - Data_Buffer[2]) * 1.5);
          }
          Switch6_Lock = Data_Buffer[3];
          if (Device6_State < Dimming_Preset_1) {
            Dimming2_Preset_Position = 0;
          } else if (Device6_State >= Dimming_Preset_1
                     && Device6_State < Dimming_Preset_2) {
            Dimming2_Preset_Position = 1;
          } else if (Device6_State >= Dimming_Preset_2
                     && Device6_State < Dimming_Preset_3) {
            Dimming2_Preset_Position = 2;
          } else {
            Dimming2_Preset_Position = 3;
          }
          break;
      }
    }

    /////////////////////feedback from controller
    /*

      UART_TXC(Data_Buffer[1]);
      if ((Data_Buffer[2]) == 0) {
      UART_TXC('*');
      if (Data_Buffer[3] == 0) {
        UART_TXC('*'); //UART_TXC('|');
      } else {
        UART_TXC(Data_Buffer[3]); //UART_TXC('|');
      }
      } else {
      UART_TXC(Data_Buffer[2]);
      if (Data_Buffer[3] == 0) {
        UART_TXC('*'); //UART_TXC('|');
      } else {
        UART_TXC(Data_Buffer[3]); //UART_TXC('|');
      }
      }
      UART_TXC(End_Byte);
    */
    //feedack from controller for Wifi command 
    //uncommetn this three lines to get feedbcak form the mqtt command 
    //Send_Device_State = 1;
    //Transmiting == 0;
    //Send_Switch_State(Data_Buffer[1], Data_Buffer[2], Data_Buffer[3],'q') ;

    Data_Processing = 0;
    Data_Position = 0;
    memset(&Data_Buffer, 0, 5);

  }
}

void Check_Switch_State() {
  if (Debounce_Check == 0) {
    if (Switch1_Lock == 0 && (!(PINB & (1 << Switch1)))
        && Switch1_Debounce_State == 1) {
      Debounce_Check = 1;
      Switch1_Debounce_State = 0;
    } else if (Switch1_Lock == 0 && ((PINB & (1 << Switch1)))
               && Switch1_Debounce_State == 0) {
      Debounce_Check = 1;
      Switch1_Debounce_State = 1;
    } else if (Switch2_Lock == 0 && (!(PINB & (1 << Switch2)))
               && Switch2_Debounce_State == 1) {
      Debounce_Check = 1;
      Switch2_Debounce_State = 0;
    } else if (Switch2_Lock == 0 && ((PINB & (1 << Switch2)))
               && Switch2_Debounce_State == 0) {
      Debounce_Check = 1;
      Switch2_Debounce_State = 1;
    } else if (Switch3_Lock == 0 && (!(PINB & (1 << Switch3)))
               && Switch3_Debounce_State == 1) {
      Debounce_Check = 1;
      Switch3_Debounce_State = 0;
    } else if (Switch3_Lock == 0 && ((PINB & (1 << Switch3)))
               && Switch3_Debounce_State == 0) {
      Debounce_Check = 1;
      Switch3_Debounce_State = 1;
    } else if (Switch4_Lock == 0 && (!(PINB & (1 << Switch4)))
               && Switch4_Debounce_State == 1) {
      Debounce_Check = 1;
      Switch4_Debounce_State = 0;
    } else if (Switch4_Lock == 0 && ((PINB & (1 << Switch4)))
               && Switch4_Debounce_State == 0) {
      Debounce_Check = 1;
      Switch4_Debounce_State = 1;
    } else if (Switch5_Lock == 0 && (!(PINB & (1 << Switch5)))
               && Switch5_Debounce_State == 1) {
      Debounce_Check = 1;
      Switch5_Debounce_State = 0;
    } else if (Switch5_Lock == 0 && ((PINB & (1 << Switch5)))
               && Switch5_Debounce_State == 0) {
      Debounce_Check = 1;
      Switch5_Debounce_State = 1;
    } else if (Switch6_Lock == 0 && (!(PINB & (1 << Switch6)))
               && Switch6_Debounce_State == 1) {
      Debounce_Check = 1;
      Switch6_Debounce_State = 0;
    } else if (Switch6_Lock == 0 && ((PINB & (1 << Switch6)))
               && Switch6_Debounce_State == 0) {
      Debounce_Check = 1;
      Switch6_Debounce_State = 1;
    }
    Debounce_Counter = 0;
  }
  if (Debounce_Check == 1 && Debounce_Counter > Debounce_Threshold_Delay) {
    Debounce_Counter = 0;
    Debounce_Check = 0;
  }
  Debounce_Counter++;
}

void Switch_State_Check() {
  if (Debounce_Check == 1 && Debounce_Counter > Debounce_Delay) {
    if (Switch1_Lock == 0 && (!(PINB & (1 << Switch1)))
        && Switch1_State == 0 && Switch1_Debounce_State == 0) {
      Switch1_State = 1;
      Device1_State = 1;
      PORTC |= (1 << Relay1);
      Send_Device_State = 1;
      Send_Switch_State(1, Device1_State, 'p','q');
    } else if (Switch1_Lock == 0 && ((PINB & (1 << Switch1)))
               && Switch1_State == 1 && Switch1_Debounce_State == 1) {
      Switch1_State = 0;
      Device1_State = 0;
      PORTC &= ~(1 << Relay1);
      Send_Device_State = 1;
      Send_Switch_State(1, Device1_State, 'p','q');
    } else if (Switch2_Lock == 0 && (!(PINB & (1 << Switch2)))
               && Switch2_State == 0 && Switch2_Debounce_State == 0) {
      Switch2_State = 1;
      Device2_State = 1;
      PORTC |= (1 << Relay2);
      Send_Device_State = 1;
      Send_Switch_State(2, Device2_State, 'p','q');
    } else if (Switch2_Lock == 0 && ((PINB & (1 << Switch2)))
               && Switch2_State == 1 && Switch2_Debounce_State == 1) {
      Switch2_State = 0;
      Device2_State = 0;
      PORTC &= ~(1 << Relay2);
      Send_Device_State = 1;
      Send_Switch_State(2, Device2_State, 'p','q');
    } else if (Switch3_Lock == 0 && (!(PINB & (1 << Switch3)))
               && Switch3_State == 0 && Switch3_Debounce_State == 0) {
      Switch3_State = 1;
      Device3_State = 1;
      PORTC |= (1 << Relay3);
      Send_Device_State = 1;
      Send_Switch_State(3, Device3_State, 'p','q');
    } else if (Switch3_Lock == 0 && ((PINB & (1 << Switch3)))
               && Switch3_State == 1 && Switch3_Debounce_State == 1) {
      Switch3_State = 0;
      Device3_State = 0;
      PORTC &= ~(1 << Relay3);
      Send_Device_State = 1;
      Send_Switch_State(3, Device3_State, 'p','q');
    } else if (Switch4_Lock == 0 && (!(PINB & (1 << Switch4)))
               && Switch4_State == 0 && Switch4_Debounce_State == 0) {
      Switch4_State = 1;
      Device4_State = 1;
      PORTC |= (1 << Relay4);
      Send_Device_State = 1;
      Send_Switch_State(4, Device4_State, 'p','q');
    } else if (Switch4_Lock == 0 && ((PINB & (1 << Switch4)))
               && Switch4_State == 1 && Switch4_Debounce_State == 1) {
      Switch4_State = 0;
      Device4_State = 0;
      PORTC &= ~(1 << Relay4);
      Send_Device_State = 1;
      Send_Switch_State(4, Device4_State, 'p','q');
    } else if (Switch5_Lock == 0 && (!(PINB & (1 << Switch5)))
               && Switch5_State == 0 && Switch5_Debounce_State == 0) {
      Switch5_State = 1;
      if (Dimming1_Preset_Position > 3) {
        Dimming1_Preset_Position = 0;
      }
      Dimming1_Preset_Position++;
      if (Dimming1_Preset_Position == 1) {
        Device5_State = Dimming_Preset_1;
        Dimmer1_Value = ((100 - Dimming_Preset_1) * 1.5);
        Dimmer1_Control = 1;
        Send_Device_State = 1;
        Send_Switch_State(5, Device5_State, 'p','q');
      } else if (Dimming1_Preset_Position == 2) {
        Device5_State = Dimming_Preset_2;
        Dimmer1_Value = ((100 - Dimming_Preset_2) * 1.5);
        Dimmer1_Control = 1;
        Send_Device_State = 1;
        Send_Switch_State(5, Device5_State, 'p','q');
      } else if (Dimming1_Preset_Position == 3) {
        Device5_State = Dimming_Preset_3;
        Dimmer1_Value = ((100 - Dimming_Preset_3) * 1.5);
        Dimmer1_Control = 1;
        Send_Device_State = 1;
        Send_Switch_State(5, Device5_State, 'p','q');
      } else {
        Device5_State = 100;
        PORTC |= (1 << Dimmer1);
        Dimmer1_Control = 0;
        Send_Device_State = 1;
        Send_Switch_State(5, Device5_State, 'p','q');
      }
    } else if (Switch5_Lock == 0 && ((PINB & (1 << Switch5)))
               && Switch5_State == 1 && Switch5_Debounce_State == 1) {
      Switch5_State = 0;
      Device5_State = 0;
      Dimmer1_Control = 0;
      PORTC &= ~(1 << Dimmer1);
      Send_Device_State = 1;
      Send_Switch_State(5, Device5_State, 'p','q');
    } else if (Switch6_Lock == 0 && (!(PINB & (1 << Switch6)))
               && Switch6_State == 0 && Switch6_Debounce_State == 0) {
      Switch6_State = 1;
      if (Dimming2_Preset_Position > 3) {
        Dimming2_Preset_Position = 0;
      }
      Dimming2_Preset_Position++;
      if (Dimming2_Preset_Position == 1) {
        Device6_State = Dimming_Preset_1;
        Dimmer2_Value = ((100 - Dimming_Preset_1) * 1.5);
        Dimmer2_Control = 1;
        Send_Device_State = 1;
        Send_Switch_State(6, Device6_State, 'p','q');
      } else if (Dimming2_Preset_Position == 2) {
        Device6_State = Dimming_Preset_2;
        Dimmer2_Value = ((100 - Dimming_Preset_2) * 1.5);
        Dimmer2_Control = 1;
        Send_Device_State = 1;
        Send_Switch_State(6, Device6_State, 'p','q');
      } else if (Dimming2_Preset_Position == 3) {
        Device6_State = Dimming_Preset_3;
        Dimmer2_Value = ((100 - Dimming_Preset_3) * 1.5);
        Dimmer2_Control = 1;
        Send_Device_State = 1;
        Send_Switch_State(6, Device6_State, 'p','q');
      } else {
        Device6_State = 100;
        PORTC |= (1 << Dimmer2);
        Dimmer2_Control = 0;
        Send_Device_State = 1;
        Send_Switch_State(6, Device6_State, 'p','q');
      }
    } else if (Switch6_Lock == 0 && ((PINB & (1 << Switch6)))
               && Switch6_State == 1 && Switch6_Debounce_State == 1) {
      Switch6_State = 0;
      Device6_State = 0;
      Dimmer2_Control = 0;
      PORTC &= ~(1 << Dimmer2);
      Send_Device_State = 1;
      Send_Switch_State(6, Device6_State, 'p','q');
    }
    Debounce_Check = 0;
  }
}



void Get_and_Update_Switch_State() {
  if (!(PINB & (1 << Switch1))) {
    _delay_ms(100);
    if (!(PINB & (1 << Switch1))) {
      Switch1_State = 1;
      Switch1_Debounce_State = 0;
      PORTC |= (1 << Relay1);
      //  Send_Switch_State(1, Switch1_State);
    }
  } else if ((PINB & (1 << Switch1))) {
    _delay_ms(100);
    if ((PINB & (1 << Switch1))) {
      Switch1_State = 0;
      Switch1_Debounce_State = 1;
      PORTC &= ~(1 << Relay1);
      //  Send_Switch_State(1, Switch1_State);
    }
  }

  if (!(PINB & (1 << Switch2))) {
    _delay_ms(100);
    if (!(PINB & (1 << Switch2))) {
      Switch2_State = 1;
      Switch2_Debounce_State = 0;
      PORTC |= (1 << Relay2);
      //    Send_Switch_State(2, Switch2_State);
    }
  } else if ((PINB & (1 << Switch2))) {
    _delay_ms(100);
    if ((PINB & (1 << Switch2))) {
      Switch2_State = 0;
      Switch2_Debounce_State = 1;
      PORTC &= ~(1 << Relay2);
      //  Send_Switch_State(2, Switch2_State);
    }
  }

  if (!(PINB & (1 << Switch3))) {
    _delay_ms(100);
    if (!(PINB & (1 << Switch3))) {
      Switch3_State = 1;
      Switch3_Debounce_State = 0;
      PORTC |= (1 << Relay3);
      //  Send_Switch_State(3, Switch3_State);
    }
  } else if ((PINB & (1 << Switch3))) {
    _delay_ms(100);
    if ((PINB & (1 << Switch3))) {
      Switch3_State = 0;
      Switch3_Debounce_State = 1;
      PORTC &= ~(1 << Relay3);
      //  Send_Switch_State(3, Switch3_State);
    }
  }

  if (!(PINB & (1 << Switch4))) {
    _delay_ms(100);
    if (!(PINB & (1 << Switch4))) {
      Switch4_State = 1;
      Switch4_Debounce_State = 0;
      PORTC |= (1 << Relay4);
      //  Send_Switch_State(4, Switch4_State);
    }
  } else if ((PINB & (1 << Switch4))) {
    _delay_ms(100);
    if ((PINB & (1 << Switch4))) {
      Switch4_State = 0;
      Switch4_Debounce_State = 1;
      PORTC &= ~(1 << Relay4);
      //    Send_Switch_State(4, Switch4_State);
    }
  }

  if (!(PINB & (1 << Switch5))) {
    _delay_ms(100);
    if (!(PINB & (1 << Switch5))) {
      Switch5_State = 1;
      Switch5_Debounce_State = 0;
      PORTC |= (1 << Dimmer1);
      Dimmer1_Control = 0;
      //    Send_Switch_State(5, Device5_State);
    }
  } else if ((PINB & (1 << Switch5))) {
    _delay_ms(100);
    if ((PINB & (1 << Switch5))) {
      Switch5_State = 0;
      Switch5_Debounce_State = 1;
      PORTC &= ~(1 << Dimmer1);
      Dimmer1_Control = 0;
      //    Send_Switch_State(5, Device5_State);
    }
  }

  if (!(PINB & (1 << Switch6))) {
    _delay_ms(100);
    if (!(PINB & (1 << Switch6))) {
      Switch6_State = 1;
      Switch6_Debounce_State = 0;
      PORTC |= (1 << Dimmer2);
      Dimmer2_Control = 0;
      ///   Send_Switch_State(6, Device6_State);
    }
  } else if ((PINB & (1 << Switch6))) {
    _delay_ms(100);
    if ((PINB & (1 << Switch6))) {
      Switch6_State = 0;
      Switch6_Debounce_State = 1;
      PORTC &= ~(1 << Dimmer2);
      Dimmer2_Control = 0;
      //    Send_Switch_State(6,Device6_State);
    }
  }

}

void Send_Switch_State(uint8_t Switch_Number, uint8_t Switch_State, uint8_t p_Character , uint8_t q_Character) {

  if (Send_Device_State == 1 && Transmiting == 0) {

    uint8_t data = 0;
    data = Switch_Number;
    Sending_Data_Buffer[0] = data;
    
    if ((Switch_State) == 0)
    {
      Sending_Data_Buffer[1] = '*';
    }
    else {
      Sending_Data_Buffer[1] = Switch_State;
    }

    if (p_Character == 'p')
    {
      Sending_Data_Buffer[2] = 'p';
    }
    else {

      if (p_Character == 0)
      {
        Sending_Data_Buffer[2] = '*';
      }
      else {
        Sending_Data_Buffer[2] = p_Character;
      }

    }

        if (q_Character == 'q')
    {
      Sending_Data_Buffer[3] = 'q';
    }
    else {

      if (q_Character == 0)
      {
        Sending_Data_Buffer[3] = '*';
      }
      else {
        Sending_Data_Buffer[3] = q_Character;
      }

    }

    Sending_Data_Buffer[4] = End_Byte;
    Transmiting = 1;
    Sending_Buffer_Position = 0;
    UART_TXC(Sending_Data_Buffer[0]);
    Send_Device_State = 0;

  }

}

/*
  void WDT_Init() {
  WDTCR |= (1 << WDCE) | (1 << WDE);
  WDTCR &= ~(1 << WDCE);
  WDTCR |= (1 << WDP2) | (1 << WDP1) | (1 << WDP0);
  WDTCR |= (1 << WDE);
  }
*/

int main(void) {
  DDRB = 0x00;
  DDRC = 0xFF;

  External_Interrupt_Enable();
  Timer_Init();
  UART_Init(103);
  wdt_enable(WDTO_2S);

  sei();

  Get_and_Update_Switch_State();

  while (1) {
    wdt_reset();
    UART_Date_Processing();
    Check_Switch_State();
    Switch_State_Check();
    //    Send_Device_States();

    if (TCNT0 == Dimmer1_Value && Dimmer1_Control == 1) {
      PORTC |= (1 << Dimmer1);
    }

    if (TCNT0 == Dimmer2_Value && Dimmer2_Control == 1) {
      PORTC |= (1 << Dimmer2);
    }

    if (TCNT0 == 152) {
      if (Dimmer1_Control == 1) {
        PORTC &= ~(1 << Dimmer1);
      }
      if (Dimmer2_Control == 1) {
        PORTC &= ~(1 << Dimmer2);
      }
    }
  }
}

